package pl.sda.jp.miniblog12client;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PostDto {
    private Long id;
    private String title;

}
