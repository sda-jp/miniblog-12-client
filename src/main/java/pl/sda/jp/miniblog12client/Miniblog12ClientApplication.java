package pl.sda.jp.miniblog12client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Miniblog12ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Miniblog12ClientApplication.class, args);
    }

}
